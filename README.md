# Google Keep to Evernote notes converter

Originally created and posted by user **dgc** on https://discussion.evernote.com/topic/97201-how-to-transfer-all-the-notes-from-google-keep-to-ever
note/

At the above URL, you can also find the motto and instructions for this script.

## Features
* Convert notes :)
* Includes embedded images  
    * Image types tested include .png and .jpg.
    * Handles multiple attachments
* Applies tags

## Usage

1. Make sure you have Python 3 installed
2. Install `parsedatetime` module
```
pip3 install parsedatetime
```
2. Download your Google Keep notes with Google Takeout
3. Extract the Google Takeout zip and copy this script to the `Takeout` folder. The script will parse all .html files in the `Keep` folder in the current directory.
4. Run the script
```
python ./keep-to-enex.py
```

Optionally, pass "-o <output_file>.enex" or "-o sys.stdout" to output to the console. If not specified, this script will write a file named "keep.enex" into the working directory.
```
python ./keep-to-enex.py -o <output_file>.enex
```

Usage can also be printed by passing "-h" at the command line.


:+1: Tested ok under Windows, Mac and Linux

I've never tested this with more than 800 notes, but anyway this has worked for me.

I made this public 'cause I feel it might be useful for more people, but this is far from a beautiful code. I *might* modify it in case it can't help anyone in its current state.
